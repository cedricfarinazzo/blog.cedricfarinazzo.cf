---
title: Hello World!
date: "2020-09-20T23:42:42.000Z"
description: "My first post!"
---

![Hello World](./hello-world.jpg)

Hey!

This is my first post on my new blog!

I will write more posts on the future about various subjects.

In the meantime, check and follow my github accound [here](https://github.com/cedricfarinazzo). You can found all my open-source project.

This blog is hosted on Gitlab ([here](https://gitlab.com/cedricfarinazzo/blog.cedricfarinazzo.cf)) and deployed with Gitlab Pages.

Good night!
Stay safe, clean code!