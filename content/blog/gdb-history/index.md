---
title: Enable GDB command history
date: "2020-09-23T23:42:42.000Z"
description: "How to enable gdb command history?"
---

![gdb logo](./gdb-logo.jpg)


By default, command history is disabled. It can be activated using the GDB configuration file: ``.gdbinit``

You can add the following command to your ``.gdbinit`` to enable command history:
```gdb
set history save on
```

Beware that gdp will create a ``.gdb_history`` file where you run gdb.
I recommend to add this file to your ``.gitignore``

Good night!
Stay safe, clean code!